#include <functional>
#include <iostream>
#include <chrono>
#include <vector>
#include <thread>
#include <future>

class JobExecutor {
    // Vector to hold "futures" from async tasks fired
    std::vector<std::shared_ptr<std::future<void>>> futures;

    /**
     * Function to wait N milliseconds and then launch the job.
     * Will be called once for every job scheduled.
     * Needs to be declared static to be called as an async task or a thread.
     */
    static void sleep_and_launch(std::function<void()> job, uint32_t milliseconds) {
        std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds));
        // Start 'job' asynchronously
        std::async(std::launch::async, job);
        // Alternate way, using thread instead of async. Start 'job' in a new thread and wait for it to finish.
        // std::thread(job).join();
    }
public:
    /**
    * Starts the executor. Returns immediately.
    */
    void start() {
        std::cout << "Starting the scheduler" << std::endl;
        // Clear any existing "futures"
        futures.clear();
    };

    /**
    * Executes/Schedules the job "job" after "milliseconds "
    * It will return immediately.
    */
    void execute(std::function<void()> job, uint32_t milliseconds) {
        // Launch the 'sleep _and_launch' function asynchronously, providing it the job and scheduled delay,
        // and make a shared pointer to hold the future returned.
        auto f = std::make_shared<std::future<void>>(std::async(std::launch::async, sleep_and_launch, job, milliseconds));
        // Store the shared pointer to the future in the vector
        futures.push_back(f);
        // We don't do a f.wait() here since we want to return from execute immediately
        //std::cout << "Returning from execute" << std::endl;
    }

    /**
    * Will return after all the jobs have been executed
    */
    void stop() {
        std::cout << "Waiting for jobs to finish" << std::endl;
        // Wait for each future to return
        for (auto f: futures)
            f->wait();
        std::cout << "All jobs stopped." << std::endl;
    };
};

int main() {
    JobExecutor executor;
    executor.start();
    for (unsigned int i = 0; i < 5; i++) {
        // Capture current time right before scheduling the job
        std::chrono::time_point<std::chrono::system_clock> schedule_time_variable = std::chrono::system_clock::now();
        uint32_t execute_after_ms = (6 - i) * 1000;
        executor.execute([i, schedule_time_variable, execute_after_ms] {
            // Get current time when the lambda (job) gets called
            std::chrono::time_point<std::chrono::system_clock> execution_time_variable = std::chrono::system_clock::now();
            // Calculate difference between scheduled and executed time points
            uint32_t executed_after_ms =
                    std::chrono::duration_cast<std::chrono::milliseconds>(execution_time_variable - schedule_time_variable).count();
            std::cout << "Job # " << i
                      << ", Scheduled after ms: " << execute_after_ms
                      << ", Executed after ms: " << executed_after_ms
                      << std::endl;
            // Added an optional sleep to simulate long-running jobs of varying lengths
            std::this_thread::sleep_for(std::chrono::seconds(rand()%20));
            std::cout << "Finished job #" << i << std::endl;
        }, execute_after_ms);
    }
    executor.stop();
    return 0;
}