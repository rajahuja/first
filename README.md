# README #

### What is this repository for? ###

* I created this repository initially because I needed a way to share my finished code for the JobScheduler programming problem.
* I then re-used it for a way to share my code for the LRU problem.

### How do I get set up? ###

* Use Linux, MacOS or Cygwin, for example:
```
$ uname
CYGWIN_NT-6.1
```

* Ensure you have a recent GCC C++ compiler installed, which supports C++11, for example:
```
$ g++ --version
g++ (GCC) 5.4.0
```

* Clone this repo onto your machine
```
$ git clone https://rajahuja@bitbucket.org/rajahuja/first.git
```

* Build and run the code
```
$ cd LRU
$ g++ LRU.cpp -std=c++11 -o TestLRU
$ ./TestLRU
```
* Sample output is in "Sample Output.txt"

```
$ cd JobScheduler
$ g++ JobScheduler.cpp -std=c++11 -o JobScheduler
$ ./JobScheduler
```


### Who do I talk to? ###

* Rajat Ahuja <rajat@ahuja.name>