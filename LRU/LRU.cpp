/*
 * (C) Copyright 2016 Rajat Ahuja <rajat@ahuja.name>
 *
 */

#include <iostream>
#include <vector>
#include <assert.h>

using namespace std;

// Key-value pairs stored in cache
typedef std::pair<int, void*> Node;

// LRU Cache class
class LRU {
    int size;
    vector<Node> cache;
    friend class LRU_Test; // So that LRU_Test class can access cache directly to test
public:
    LRU(int sz): size(sz) {};
    void* get(int key);
    void set(int key, void* val);
};

void* LRU::get(int key) {
    int count = 0;
    for (auto node : cache) {
        count++;
        if (node.first == key) {
            cache.erase(cache.begin()+count-1); //Erase node from where it exists
            cache.push_back(node);  // Add node to end
            return node.second;
        }
    }
    return nullptr;
}

void LRU::set(int key, void *val) {
    // Check if key already exists, to ensure unique key
    if (get(key)) {
        // If get succeeds, that element is already at the end
        // Update its value
        cache.back().second = val;
        return;
    }
    // Evict LRU node if cache full
    if (cache.size() == size) {
        cache.erase(cache.begin());
    }
    auto node = make_pair(key, val);
    cache.push_back(node);
}

// A class to test LRU
class LRU_Test {
    LRU *lru;
    vector<void*> vs;
public:
    LRU_Test() { lru = new LRU(5);}
    void test_get();
    void test_set();
    void test_get_without_set();
    static void print_cache(vector<pair<int, void*>> &cache);
    ~LRU_Test() {
        // Cleanup memory allocated in ctor, or during set
        for (auto v: vs) {
            if (v)
                free(v);
        }
        delete lru;
    }
};

// Print each node in a cache
void LRU_Test::print_cache(vector<pair<int, void*>> &cache) {
    cout << "Cache: ";
    for (auto node : cache)
        cout << node.first << ' ';
    cout << endl;
}

// Tests LRU::get()
void LRU_Test::test_get() {
    for (int i=0; i<10; i++) {
        void *p = lru->get(i);
        cout << "Looking for " << i << ": ";
        if (p) {
            cout << "SUCCESS; ";
            assert(lru->cache.back().first == i);   // Last element must be same as last get element
        } else
            cout << "ERROR; ";
        print_cache(lru->cache);
    }
    cout << endl;
}

// Tests LRU::get without using LRU::set() first
void LRU_Test::test_get_without_set() {
    void* v;
    for (int i=0; i<5; i++) {
        v = (void*) new int(1);
        Node node(i, v);
        vs.push_back(v);
        lru->cache.push_back(node);
    }
    print_cache(lru->cache);
    test_get();
}

// Tests LRU::set()
void LRU_Test::test_set() {
    void* v;
    for (int i=0; i<10; i++) {
        v = (void*) new int(1);
        cout << "Setting key " << i << "; ";
        lru->set(i, v);
        print_cache(lru->cache);
        assert(lru->cache.size() <= lru->size); // Size must not exceed predefined cache-size
        assert(lru->cache.back().first == i);   // Last element must be same as last set element
    }
    // Try inserting some non-unique keys
    for (int i=6; i<9; i++) {
        v = (void*) new int(1);
        cout << "Setting key " << i << "; ";
        lru->set(i, v);
        print_cache(lru->cache);
        assert(lru->cache.size() <= lru->size); // Size must not exceed predefined cache-size
        assert(lru->cache.back().first == i);   // Last element must be same as last set element
    }
    cout << endl;
}

int main() {
    LRU_Test lru_test;
    cout << endl;
    cout << "Testing LRU::get() without calling set first" << endl;
    lru_test.test_get_without_set();
    cout << "Testing LRU::set()" << endl;
    lru_test.test_set();
    cout << "Testing LRU::get()" << endl;
    lru_test.test_get();
    return 0;
}