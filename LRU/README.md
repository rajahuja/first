# README #

### What is this repository for? ###

* I created this repository because I needed a way to share my finished code for the LRU programming problem.

### How do I get set up? ###

* Use Linux, MacOS or Cygwin, for example:
```
$ uname
Darwin
```

* Ensure you have a recent GCC C++ compiler installed, which supports C++11, for example:
```
$ g++ --version
Apple LLVM version 7.3.0 (clang-703.0.31)
```

* Clone this repo onto your machine
```
$ git clone https://rajahuja@bitbucket.org/rajahuja/second.git
```

* Build and run the code
```
$ cd LRU
$ g++ LRU.cpp -std=c++11 -o TestLRU
$ ./TestLRU
```
* Sample output is in "Sample Output.txt"

### Who do I talk to? ###

* Rajat Ahuja <rajat@ahuja.name>